//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
  {"","/home/odea/.local/bin/statusbar/sb-mailbox",5,9},
  {"","/home/odea/.local/bin/aptupdate",60,6},
  //{"  ","mpstat | awk '$12 ~ /[0-9.]+/ { print 100 - $12\"%\" }'",2,1},
  //{"","echo -n ' XMR: ' && curl http://eur.rate.sx/1xmr | cut -c1-6",360,11,},
  {"","/home/odea/.local/bin/statusbar/sb-cpu3",3,1},
  //{"", "/home/odea/.local/bin/statusbar/sb-memory2",	60,		2},
  {"", "echo \"  `free | grep Mem | awk '{print $3/$2 * 100.0}' | cut -f1 -d\".\"`%\"",	60,		2},
  {" ", "df / --output=pcent | tail -n 1 | tr -d \" \"",	60,		3},
  {"","/home/odea/.local/bin/vpn",5,10},
  //{"","/home/odea/.local/bin/statusbar/sb-nettraf",2,3},
  //{"[BT]","",0,5},
  //{"BAT:90%","",60,4},
  {"","/home/odea/.local/bin/volume",0,10},
  {"","curl -H \"Accept-Language: fr\" wttr.in/Vincennes?format=\"%C\" && printf \" \" && /home/odea/.local/bin/meteocielsaintmaur",360,11},
  {"", "date '+%a %d %b %H:%M'",2,8},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ⎪ ";
static unsigned int delimLen = 5;
